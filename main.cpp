/**
 * @file  main.cpp
 *    @author   Ava Cole
 *    @author   Sarah Hansen
 *    @date     25-Febuary
 *    @brief    ...
 * 
 *    detailed description ...
 */

#include <Arduino.h>
#include <Wire.h>
//#include <Serial.h>

#include "segmentDisplay.h"
#include "task1.h"
#include "task2.h"
#include "diyTone.h"
#include "rrSched.h"
#include "srriSched.h"

void setup() {
  Task1Setup();
  ToneSetup();
  SegmentDisplaySetup();
}

void loop() {
  // demo 1
  //RRSched();

  // demo 2
  void (*Demo2taskQueue[NTASKS]) () = {
    Task1Sleepy,
    Task2Sleepy,
    schedule_sync,
    NULL
  };
  SRRISched(Demo2taskQueue);

  // demo 3
  /*
  TODO: TCB list or something {
    Task1,
    Task2,
  }
  DDSched();*/

  // demo 4
  /*void (*Demo4taskQueue[NTASKS])() = {
    Task1,
    Task2,
    TODO: Task3,
    schedule_sync,
    NULL,
  };
  SRRISched(Demo4taskQueue);*/

  // demo 5
  /* TODO: TCB list or something {
    Task4
  }
  DDSched();*/

  // demo 6
  /* TODO: TCB list or someting {
    Task5
  }
  DDSched();*/
}


