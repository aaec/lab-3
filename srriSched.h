/**
 * @file  srriSched.h
 *    @author   Ava Cole
 *    @author   Sarah Hansen
 *    @date     25-Febuary
 *    @brief    ...
 * 
 *    detailed description ...
 */

#include <Arduino.h>

// the max number of tasks our scheduler can handle, INCLUDING schedule_sync
#define NTASKS 10

/***************************************************
 * @brief remove task from schedule queue for t milliseconds
 * 
 * 			@param t Number of milliseconds to sleep for.
 * 
 * 			The task this function is being called from
 * 			will not be scheduled for the next t milliseconds.
 * 
 * 			@warning t is only accurate to 2 milliseconds
 *
 */
void sleep_474(int t);

/***************************************************
 * @brief schedule queue of function pointers
 * 
 * 			@param taskQueue An array	containing task
 * 				functions to be scheduled. The last task
 * 				must be scheduler_sync. The last task
 * 				must be followed by NULL.
 * 
 * 			This scheduler will schedule each task in the
 * 			taskQueue every 2 milliseconds as long as the
 * 			combined duration of all the tasks does not
 * 			exceed 2 milliseconds.
 *
 */
void SRRISched(void (*taskQueue[NTASKS])());
