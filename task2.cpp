/**
 * @file  task2.cpp
 *    @author   Ava Cole
 *    @author   Sarah Hansen
 *    @date     25-Febuary
 *    @brief    ...
 * 
 *    detailed description ...
 */

#include "diyTone.h"
#include "task2.h"
#include "srriSched.h"

const int NUM_NOTES = 9;
const int TONE_DURATION = 1000;
const int MELODY[] = {NOTE_D4, NOTE_E4, NOTE_C4, NOTE_C3, NOTE_G3, NOTE_R, NOTE_R, NOTE_R, NOTE_R};

int curNote = 0;

void Task2() {
  if (curNote >= NUM_NOTES) {
    curNote = 0;
  }
  diyTone(MELODY[curNote++]);
}

void Task2Sleepy() {
    if (curNote >= NUM_NOTES) {
    curNote = 0;
  }
  diyTone(MELODY[curNote++]);
  sleep_474(1000);
}