/**
 * @file  srriSched.cpp
 *    @author   Ava Cole
 *    @author   Sarah Hansen
 *    @date     25-Febuary
 *    @brief    ...
 * 
 *    detailed description ...
 */

#include "srriSched.h"

// Possible states a task can be in.
enum state {
	READY,
	RUNNING,
	SLEEPING
};

// Possible states the sFlag variable can be in,
// either waiting for an interrupt to sync to (pending)
// , or ready to sync to the latest interrupt (pending)
enum sFlagState {
	PENDING,
	DONE
};

// We initialize the state of the scheduler to be pending
// an interrupt to sync to.
volatile int sFlag = PENDING;

// This is our queue of tasks to schedule, given to the
// scheduler by the function that calls SRRISched.
void (*taskQueue[NTASKS]) ();

// Here we track the state of each task in the taskQueue.
int taskStates[NTASKS];

// Here we track the number of milliseconds (sleepies)
// a task has left in it's sleeping state.
int taskSleepies[NTASKS];

/***************************************************
 * @brief sync da scheduler
 * 
 * 			Wait for an interrupt to update sFlag then updates
 * 			timers for all sleeping tasks and changes state to
 * 			READY for all tasks that are done sleeping.
 *
 */
void schedule_sync() {
	// wait for that sweet sweet interrupt
	while(sFlag == PENDING);
	// decriment all sleep times by 2ms
	for (int i = 0; taskQueue[i] != NULL; i++) {
		if (taskStates[i] == SLEEPING) {
			// wake up any sleeping tasks whose sleep time is 0
			if (taskSleepies[i] < 2) {
				taskStates[i] = READY;
			} else {
				taskSleepies[i] -= 2;
			}
		}
	}
	// were need another interrupt before we can sync again!
	sFlag = PENDING;
}

/***************************************************
 * @brief update the state of the program on interrupt
 * 
 * 			When an interrupt is recieved from the timer,
 * 			update sFlag so that our scheduler can syncronize
 * 			with the interrupt.
 *
 */
ISR(TIMER0_COMPA_vect) {
	sFlag = DONE;
}

void sleep_474(int t) {
	// find task that is currently running
	for (int i = 0; taskQueue[i] != NULL; i++) {
		if (taskStates[i] == RUNNING) {
			// change it's state to sleeping
			taskStates[i] = SLEEPING;
			// update it's sleepy to t
			taskSleepies[i] = t;
		}
	}
}

void SRRISched(void (*tasks[NTASKS])()) {
	// copy task array
	int i;
	for (i = 0; tasks[i] != NULL; i++) {
		taskQueue[i] = tasks[i];
	}
	taskQueue[i] = NULL;

	// initialize taskStates to READY
	for (int i = 0; taskQueue[i] != NULL; i++) {
		taskStates[i] = READY;
	}

	// Setup timer for interrupt. We are using timer 0.
	cli(); // disable interupts
  //TIMSK0 = 0;   // disable interrupts
  TCCR0A = 0;   // normal counting mode
  TCCR0B = 0;   // timer mode
  TCNT0  = 0;   //initialize counter value to 0
  // set prescaller to 256
  TCCR0B |= (1 << CS02); 
	// Note: OCR = [F_CPU /(prescaler * freq)]-1
  // we want intterupts every 2ms so freq = 500hz
  OCR0A = 124; //(float) F_CPU / (256 * 500) - 1;
  // Set timer to CTC mode
  TCCR0A |= (1 << WGM01);
  // enable timer compare interrupt
  TIMSK0 |= (1 << OCIE0A);
	sei(); // reenable interuppts

	// call each task in turn
	while(true) {
		for (int i = 0; taskQueue[i] != NULL; i++) {
			if (taskStates[i] == READY) {
				taskStates[i] = RUNNING;
				taskQueue[i]();
				// it's possible it fell asleep while running, u never know!
				if (taskStates[i] == RUNNING) {
					taskStates[i] = READY;
				}
			}
		}
	}
}
