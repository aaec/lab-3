/**segmentDisplay.cpp
 * @file	segmentDisplay.cpp
 *		@author		Ava Cole
 * 		@author		Sarah Hansen
 * 		@date			25-Febuary
 * 		@brief		...
 * 
 * 		detailed description ...
 */

#include <Arduino.h>
#include <Wire.h>


//				logical	arduino	segment
#define 	SEG_A		52		//11
#define 	SEG_B		48		//7
#define 	SEG_C		45		//4
#define 	SEG_D		43		//2
#define 	SEG_E		42		//1
#define 	SEG_F		51		//10
#define 	SEG_G		46		//5
#define 	SEG_DP	44		//3
#define 	DIG1		53		//12
#define 	DIG2		50		//9
#define 	DIG3		49		//8
#define 	DIG4		47		//6

byte DIGIT_SEGMENTS[10][7] = { { 1,1,1,1,1,1,0 },  // = 0
	                               { 0,1,1,0,0,0,0 },  // = 1
	                               { 1,1,0,1,1,0,1 },  // = 2
	                               { 1,1,1,1,0,0,1 },  // = 3
	                               { 0,1,1,0,0,1,1 },  // = 4
	                               { 1,0,1,1,0,1,1 },  // = 5
	                               { 1,0,1,1,1,1,1 },  // = 6
	                               { 1,1,1,0,0,0,0 },  // = 7
	                               { 1,1,1,1,1,1,1 },  // = 8
	                               { 1,1,1,0,0,1,1 }   // = 9
	                               };

int PINS[] = {SEG_A,SEG_B,SEG_C,SEG_D,SEG_E,SEG_F,SEG_G,SEG_DP,DIG1,DIG2,DIG3,DIG4};

void SegmentDisplaySetup() {
	for (int i = 0; i < 12; i++) {
		pinMode(PINS[i], OUTPUT);
	}
}

int SelectDigit(int digit) {
	if (digit < 1 || digit > 4) {
		Serial.println("SelectDigit(): bad input");
		return -1;
	}
	digitalWrite(DIG1, HIGH);
	digitalWrite(DIG2, HIGH);
	digitalWrite(DIG3, HIGH);
	digitalWrite(DIG4, HIGH);
	switch(digit) {
		case 1:
			digitalWrite(DIG1, LOW);
			break;
		case 2:
			digitalWrite(DIG2, LOW);
			break;
		case 3:
			digitalWrite(DIG3, LOW);
			break;
		case 4:
			digitalWrite(DIG4, LOW);
			break;
	}
}

void ClearDisplay() {
	for (int pin = 0; pin < 8; pin++) {
		digitalWrite(PINS[pin], LOW);
	}
}

int WriteDigit(int value) {
	if (value < 0 || value > 9) {
		Serial.println("WriteDigit(): bad input");
		return -1;
	}
	for (int segment = 0; segment < 7; segment++) {
		if (DIGIT_SEGMENTS[value][segment]) {
			digitalWrite(PINS[segment], HIGH);
		}
	}
}


int SegmentDisplayUInt(unsigned int value) {
	// write the thousands place
	ClearDisplay();
	SelectDigit(1);
	WriteDigit(value/1000);
	delay(5);
	ClearDisplay();
	SelectDigit(2);
	WriteDigit((value%1000)/100);
	delay(5);
	ClearDisplay();
	SelectDigit(3);
	WriteDigit((value%100)/10);
	delay(5);
	ClearDisplay();
	SelectDigit(4);
	WriteDigit(value%10);
	delay(5);
	return 0;
}

int SegmentDisplayFloat(float value) {
	return -1;
}

int SegmentDisplaySmile() {
	return -1;
}