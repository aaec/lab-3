/**segmentDisplay.h
 * @file	segmentDisplay.h
 *		@author		Ava Cole
 * 		@author		Sarah Hansen
 * 		@date			25-Febuary
 * 		@brief		...
 * 
 * 		detailed description ...
 */


/***************************************************
 * @brief prepare segment display for writing
 * 
 * 			Prepares the cathode common seven segment display
 * 			on the given pins defined in @file segmentDisplay.h
 * 			Call before attempting to write to the display
 *
 */
void SegmentDisplaySetup();

/***************************************************
 * @brief Print an unsigned int to the segment display
 *
 * 			@param value The value to print to the display.
 * 				Cannot be larger than 9999.
 * 
 * 			@return Zero on success.			
 * 
 * 			Print an unsigned integer to the seven segment
 * 			display. Call at least once a second to maintain
 * 			the appearance of a sustained value.
 *      
 */
int SegmentDisplayUInt(unsigned int value);


int SegmentDisplayFloat(float value);


int SegmentDisplaySmile();