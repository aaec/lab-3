/**
 * @file  rrSched.cpp
 *    @author   Ava Cole
 *    @author   Sarah Hansen
 *    @date     25-Febuary
 *    @brief    ...
 * 
 *    detailed description ...
 */

#include <Arduino.h>

#include "rrSched.h"
#include "task1.h"
#include "task2.h"

void RRSched() {
  while (true) { // period 1s
    Task2(); // duration ~0ms
    Task1(); // duration 250ms
    for (int i = 0; i < 750; i++) { // delay(750)
      delayMicroseconds(1000);
    }
  }
}