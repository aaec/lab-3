/**
 * @file	task1.h
 *		@author		Ava Cole
 * 		@author		Sarah Hansen
 * 		@date			25-Febuary
 * 		@brief		...
 * 
 * 		detailed description ...
 */

#define LED_PIN 41

/***************************************************
 * @brief Perform necessary setup for task 1.
 * 
 * 			Perform necessary setup for task 1.
 *
 */
void Task1Setup();

/***************************************************
 * @brief Enable LED for task 1.
 * 
 * 			Enable LED for task 1. Call every 1 second.
 *      Duration 250ms
 *
 */
void Task1();

/***************************************************
 * @brief Enable LED for task 1 using sleepy_474
 * 
 *      Enable LED for task 1.
 *
 */
void Task1Sleepy();